import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { AgendamentoPage } from './../agendamento/agendamento';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  empresa() {
    console.log("Teste")
  }

  agendar() {
   this.navCtrl.push(AgendamentoPage)
  }
  
}
