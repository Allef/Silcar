import { Component } from '@angular/core';
import {  AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';

import { HomePage } from './../home/home';

@IonicPage()
@Component({
  selector: 'page-agendamento',
  templateUrl: 'agendamento.html',
})
export class AgendamentoPage {

  // private dataExame = String;
  // private horaExame = String;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public alertCtrl: AlertController
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AgendamentoPage');
  }

  agendar() {
    const alert = this.alertCtrl.create({
      title: 'Agendamento',
      subTitle: 'Seu agendamento foi realizado com sucesso. Obrigado!',
      buttons: ['Ok']
    });
    alert.present();
    this.navCtrl.setRoot(HomePage)
  }

}
