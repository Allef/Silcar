import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { AgendamentoPage } from './../pages/agendamento/agendamento';
import { ContatoPage } from './../pages/contato/contato';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { MyApp } from './app.component';
import { SugestoesPage } from './../pages/sugestoes/sugestoes';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    AgendamentoPage,
    ContatoPage,
    MyApp,
    HomePage,
    ListPage,
    SugestoesPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    AgendamentoPage,
    ContatoPage,
    MyApp,
    HomePage,
    ListPage,
    SugestoesPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
